package ca.evolocity.poc.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RestController
public class UiPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(UiPocApplication.class, args);
	}

	@RequestMapping("/heroes")
	public List<Hero> heroes() {
		List<Hero> heroList = new ArrayList<Hero>();

		heroList.add(new Hero(1, "Achile"));
		heroList.add(new Hero(2, "Apolo"));
		heroList.add(new Hero(3, "Ares"));
		heroList.add(new Hero(4, "Arthemis"));
		heroList.add(new Hero(5, "Dionysus"));
		heroList.add(new Hero(6, "Hades"));
		heroList.add(new Hero(7, "Hermes"));
		heroList.add(new Hero(8, "Hercules"));
		heroList.add(new Hero(9, "Hestia"));
		heroList.add(new Hero(10, "Poseidon"));
		heroList.add(new Hero(11, "Zeus"));

		return heroList;
	}


	@RequestMapping("/lots/of/heroes")
	public List<Hero> lotsOfHeroes() {
		List<Hero> heroList = new ArrayList<Hero>();

		for (int i=0; i < 10000; i++) {
			heroList.add(new Hero(i + 1, "Hero".concat(Integer.toString(i))));
		}

		return heroList;
	}
}
